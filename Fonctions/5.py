#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 28 13:17:51 2018

@author: utilisateur
"""
def double_et_decuple(entiere,flottante):
    if isinstance(entiere,str):
        double = None
    else:
        double = entiere*2
    if isinstance(flottante,str):
        decuple = None
    else:
        decuple = flottante*10
    return (double, decuple)
print(double_et_decuple("a", "b"))