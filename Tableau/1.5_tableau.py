#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 30 12:45:09 2018

@author: utilisateur
"""

tab=[]
total=0
nombre=int(input("Combien voulez vous entrez de notes ? "))
for i in range (1, nombre+1):
    note=float(input("note n°"+str(i)+" : "))
    tab.append(note)
    if i==1:
        grande=tab[0]
        petite=tab[0]
    if note>grande:
        grande=note
    if note<petite:
        petite=note
    total=total+note
moyenne=total/len(tab)
print("")
print ("Les notes sont :", tab)
print("La plus petite note est :", petite)
print("La plus grande note est :", grande)
print("La Moyenne est :", round(moyenne, 2))